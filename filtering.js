// select all category labels on the research project cards
var categories = document.querySelectorAll('.w-dyn-item .card-category');

categories.forEach( function(elem) {
    var text = elem.innerText || elem.innerContent;
    if (!text) { 
        var text = 'empty';
    }

    var conv = text.replace(/[!\"#$%&'\(\)\*\+,\.\/:;<=>\?\@\[\\\]\^`\{\|\}~]/g, '');
    var className = conv.replace (/ /g, "-")
                        .toLowerCase()
                        .trim();

    // add new class to collection list item
    elem.parentElement.parentElement.parentElement.classList.add(className);
});

var mixElement = document.querySelector('.project-card-grid');
var checkboxWrapper = document.querySelector('.checkbox-filter-block');
var checkboxes = checkboxWrapper.querySelectorAll('input[type="checkbox"]');

var allCheckbox = checkboxWrapper.querySelector('input[value="all"]');

var mixer = mixitup(mixElement, {
   pagination: {
       limit: 5,
   }
});

// filter on checkbox change
checkboxWrapper.addEventListener('change', function(e) {
    var selectors = [];
    var checkbox;
    var i;

    // "all" checkbox was checked
    if (e.target === allCheckbox && e.target.checked) {
        // uncheck all other checkboxes
        for (i = 0; i < checkboxes.length; i++) {
            checkbox = checkboxes[i];
        if (checkbox !== allCheckbox) checkbox.checked = false;
      }
    // any other checkbox was changed
    } else {
        // uncheck "all" checkbox 
      allCheckbox.checked = false;
    }

    // collect all checkboxes that are checked after change
    for (i = 0; i < checkboxes.length; i++) {
      checkbox = checkboxes[i];
      if (checkbox.checked) selectors.push(checkbox.value);
    }

    // create selector string, follow "OR" logic
    // -> show all items that belongs to one category OR another
    var selectorString = selectors.length > 0 ?
        selectors.join(',')
                  :'all';           

    // apply filter
    mixer.filter(selectorString);
});

// text input search
var inputText;
var $matching = $();

$("#search-input").keyup(function(){
    inputText = $("#search-input").val().toLowerCase();

    // Check to see if input field is empty
    if ((inputText.length) > 0) {            
        $( '.mix').each(function() {
            $this = $("this");

            // add item to be filtered out if input text matches items inside the title
            console.log($(this).children().children().children('.project-card-header'));
            if($(this).children().children().children('.project-card-header').text().toLowerCase().match(inputText)) {
                $matching = $matching.add(this);
            } else {
                // removes any previously matched item
                $matching = $matching.not(this);
            }
        });

        mixer.filter($matching);
    } else {
        // resets the filter to show all item if input is empty
        mixer.filter('all');
    }
});